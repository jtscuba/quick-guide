from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
import extractText_teaser as ett
from django.template import RequestContext, loader

# Create your views here.
def index(request):
    return render(request, 'QuickGuide/index.html')

def ask(request):
	if request.method == 'POST':
		form = request.POST
		print "Response is post"

		return HttpResponseRedirect('results/')
	else:
		print "response is not post"
		form= nameForm()

	return render(request, 'QuickGuide/index.html', {
            'error_message': "You didn't enter a query",
            })

def results(request):

	if request.method == 'POST':
		rqst = request.POST.get("Query")
		if not rqst:
			return render(request, 'QuickGuide/index.html', {
            'error_message': "You didn't enter a query",
            })
		# run algorithm
		shortsummary = ett.shortSummaryFromQuery(rqst)
		tmp=""
		for sentence in shortsummary:
			tmp = tmp+" "+sentence
		shortsummary = tmp
		tmp=""

		longSummary = ett.longSummaryFromQuery(rqst)
		for sentence in longSummary:
			tmp = tmp+" "+sentence
		longSummary = tmp
		tmp=""

		wholeArcticle = ett.wholeArticleFromQuery(rqst)
		for sentence in wholeArcticle:
			tmp = tmp+" "+sentence
		wholeArcticle = tmp
		# page_summaries_list = [request.POST.get("Query"),"shortsummary","longSummary","wholeArcticle"]
		page_summaries_list = [request.POST.get("Query"),shortsummary,longSummary,wholeArcticle]

		context = {'page_summaries_list': page_summaries_list}

		return render(request, 'QuickGuide/results.html', context)

		
